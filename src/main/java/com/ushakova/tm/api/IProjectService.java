package com.ushakova.tm.api;

import com.ushakova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project removeOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

}
