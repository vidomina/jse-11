package com.ushakova.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void removeOneByName();

    void removeOneById();

    void removeOneByIndex();

    void findOneById();

    void findOneByIndex();

    void findOneByName();

    void updateTaskById();

    void updateTaskByIndex();

}
