package com.ushakova.tm.api;

import com.ushakova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
