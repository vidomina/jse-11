package com.ushakova.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void removeOneByName();

    void removeOneById();

    void removeOneByIndex();

    void findOneById();

    void findOneByIndex();

    void findOneByName();

    void updateProjectById();

    void updateProjectByIndex();

}
