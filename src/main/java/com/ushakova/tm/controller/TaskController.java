package com.ushakova.tm.controller;

import com.ushakova.tm.api.ITaskController;
import com.ushakova.tm.api.ITaskService;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        System.out.println("***Task List***");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("\n*Ok*");
    }

    @Override
    public void create() {
        System.out.println("***Task Create***\nEnter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if(task == null){
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");
    }

    @Override
    public void clear() {
        System.out.println("***Task Clear***");
        taskService.clear();
        System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneByName() {
        System.out.println("***Remove Task***\nEnter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneById() {
        System.out.println("***Remove Task***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("***Remove Task***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void findOneById() {
        System.out.println("***Show Task***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showTaskInfo(task);
        System.out.println("\n*Ok*");
    }

    @Override
    public void findOneByIndex() {
        System.out.println("***Show Task***\nEnter Index:\"");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showTaskInfo(task);
        System.out.println("\n*Ok*");
    }

    @Override
    public void findOneByName() {
        System.out.println("***Show Task***\nEnter Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showTaskInfo(task);
        System.out.println("\n*Ok*");
    }

    @Override
    public void updateTaskById() {
        System.out.println("***Update Task***\nEnter Id:\"");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");

    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("***Update Task***\nEnter Index:\n");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdated == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");
    }

    private void showTaskInfo(final Task task) {
        if(task == null) return;
        System.out.println("Id: " + task.getId() + "\nName: " + task.getName()+ "\nDescription: " + task.getDescription());
    }

}
