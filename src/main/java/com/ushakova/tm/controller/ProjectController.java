package com.ushakova.tm.controller;

import com.ushakova.tm.api.IProjectController;
import com.ushakova.tm.api.IProjectService;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("***Project List***");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("\n*Ok*");
    }

    @Override
    public void create() {
        System.out.println("***Project Create***");
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if(project == null){
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");
    }

    @Override
    public void clear() {
        System.out.println("*Project Clear*");
        projectService.clear();
        System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneByName() {
        System.out.println("***Remove Project***\nEnter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneById() {
        System.out.println("***Remove Project***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("***Remove Project***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void findOneById() {
        System.out.println("***Show Project***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showProjectInfo(project);
        System.out.println("\n*Ok*");
    }

    @Override
    public void findOneByIndex() {
        System.out.println("***Show Project***\nEnter Index:\"");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showProjectInfo(project);
        System.out.println("\n*Ok*");
    }

    @Override
    public void findOneByName() {
        System.out.println("***Show Project***\nEnter Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showProjectInfo(project);
        System.out.println("\n*Ok*");
    }

    @Override
    public void updateProjectById() {
        System.out.println("***Update Project***\nEnter Id:\"");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(id, name, description);
        if (projectUpdated == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");

    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("***Update Project***\nEnter Index:\"");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdated == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");
    }

    private void showProjectInfo(final Project project) {
        if(project == null) return;
        System.out.println("Id: " + project.getId() + "\nName: " + project.getName()+ "\nDescription: " + project.getDescription());
    }
    
}
